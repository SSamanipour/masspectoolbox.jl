using MasSpecToolBox
using Test
using Pkg


@testset "MasSpecToolBox.jl" begin
    # Write your tests here.

    pathin=pwd()
    # pathin = "/Users/saersamanipour/Desktop/dev/pkgs/MasSpecToolBox.jl/test"
    filenames=["test_chrom.mzXML"]
    mz_thresh=[0,600]
    int_thresh = 150 

    max_numb_iter=10
    max_t_peak_w=300
    res=20000
    min_ms_w=0.02
    r_thresh=0.85
    min_int=2000
    sig_inc_thresh=5
    S2N=2

    min_peak_w_s=3

  
    @warn("The feature detection of a single file is being tested.")

    method = "3DP"

    rep_table,final_table = feature_detect(pathin,filenames,mz_thresh,max_numb_iter,max_t_peak_w,res,min_ms_w,min_int,sig_inc_thresh,S2N,min_peak_w_s,method)

    @test final_table[!,"Int"][1] == 230546.0

    method = "S3DP"

    rep_table,final_table = feature_detect(pathin,filenames,mz_thresh,max_numb_iter,max_t_peak_w,res,min_ms_w,min_int,sig_inc_thresh,S2N,min_peak_w_s,method)

    @test final_table[!,"Int"][1] == 230546.0

    method = "S3DC"

    rep_table,final_table = feature_detect(pathin,filenames,mz_thresh,max_numb_iter,max_t_peak_w,res,min_ms_w,min_int,sig_inc_thresh,S2N,min_peak_w_s,method)


    @test final_table[!,"MeasMass"][1] == 199.1627

    @warn("The feature detection of multiple files is being tested.")

    output = feature_detect_batch(pathin,mz_thresh,max_numb_iter,max_t_peak_w,res,min_ms_w,min_int,sig_inc_thresh,S2N,min_peak_w_s)

    @test output == 1

 

    @warn("The feature detection of multiple files in distributed manner is being tested.")

    method = "S3DP"

    output = feature_detect_batch_dist(pathin,mz_thresh,max_numb_iter,max_t_peak_w,res,min_ms_w,min_int,sig_inc_thresh,S2N,min_peak_w_s, method, int_thresh,r_thresh)
   
    @test output == 1
end
