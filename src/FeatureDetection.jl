
using Distributed
addprocs()

using MS_Import
@everywhere using SAFD 
using ProgressBars

####################################################
"""
This function directly imports the relevant mzXML or CDF file and performs feature detection at MS1 level. Depending on the option used different algorithms can be used.
The fd_opt can be 3DP (i.e. normal 3D on profile data), S3DP (i.e. pseudo 3D on profile data), or "S3DC" for first centroiding the data and then performing feature detection on centroided data.  



"""

function feature_detect(pathin,filenames,mz_thresh,max_numb_iter,max_t_peak_w,res,min_ms_w,min_int,sig_inc_thresh,S2N,min_peak_w_s, 
    method::String= "S3DP",int_thresh::Int64 = 150,r_thresh::Float64 = 0.85)
    

    chrom = MS_Import.import_files(pathin,filenames,mz_thresh,int_thresh)
    
    #println(int_thresh)
    #println(method)

    if method == "S3DP"

        rep_table,final_table = SAFD.safd_s3D(chrom["MS1"]["Mz_values"],chrom["MS1"]["Mz_intensity" ],chrom["MS1"]["Rt" ],
            filenames[1][1:end-6],pathin,max_numb_iter,max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s);
    elseif method == "3DP"
        rep_table,final_table = SAFD.safd(chrom["MS1"]["Mz_values"],chrom["MS1"]["Mz_intensity" ],chrom["MS1"]["Rt" ],
            filenames[1][1:end-6],pathin,max_numb_iter,max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s);

    elseif method == "S3DC"

        mz_val_cent,mz_int_cent,dm_c = SAFD.centroid(chrom["MS1"]["Mz_values"],chrom["MS1"]["Mz_intensity"],min_int,res); 

        mz_vals = deepcopy(mz_val_cent)
        mz_int = deepcopy(mz_int_cent)

        mdm = deepcopy(dm_c);

        rep_table,final_table = safd_s3d_cent(mz_vals,mz_int,chrom["MS1"]["Rt"],filenames[1][1:end-6],pathin,max_numb_iter,
            max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s,"MDM",mdm)


    end 

    return rep_table,final_table
end 




####################################################
"""
This function directly imports the relevant mzXML or CDF file and performs feature detection at MS1 level. Depending on the option used different algorithms can be used.
The fd_opt can be 3DP (i.e. normal 3D on profile data), S3DP (i.e. pseudo 3D on profile data), or "S3DC" for first centroiding the data and then performing feature detection on centroided data.  



"""

function feature_detect_batch(pathin,mz_thresh,max_numb_iter,max_t_peak_w,res,min_ms_w,min_int,sig_inc_thresh,S2N,min_peak_w_s, 
    method::String= "S3DP",int_thresh::Int64 = 150,r_thresh::Float64 = 0.85)
    

   nn = readdir(pathin)
   
    
   for i in ProgressBar(1:size(nn,1))
        m = split(nn[i,1],".")
        if m[end] != "mzXML" 
            continue

        end 

        println([nn[i]])

        feature_detect(pathin,[nn[i]],mz_thresh,max_numb_iter,max_t_peak_w,res,min_ms_w,min_int,sig_inc_thresh,S2N,min_peak_w_s,method,int_thresh,r_thresh)

    end 

    return 1

    
end 



####################################################
"""
This function directly imports the relevant mzXML or CDF file and performs feature detection at MS1 level. Depending on the option used different algorithms can be used.
The fd_opt can be "3DP" (i.e. normal 3D on profile data), "S3DP" (i.e. pseudo 3D on profile data), or "S3DC" for first centroiding the data and then performing feature detection on centroided data.  


"""

function feature_detect_batch_dist(pathin,mz_thresh,max_numb_iter,max_t_peak_w,res,min_ms_w,min_int,
    sig_inc_thresh,S2N,min_peak_w_s,method,int_thresh,r_thresh)
    
    println("Files being loaded. It may take several minutes.")

    mz_vales_mat,mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat,Rt_mat = MS_Import.import_files_batch_MS1(pathin,mz_thresh,int_thresh);

    pathin_mat = repeat([pathin],size(t0_mat,1))
    Max_it=repeat([max_numb_iter],size(t0_mat,1))
    Max_t_peak_w=repeat([max_t_peak_w],size(t0_mat,1))
    Res=repeat([res],size(t0_mat,1))
    Min_ms_w=repeat([min_ms_w],size(t0_mat,1))
    R_thresh=repeat([r_thresh],size(t0_mat,1))
    Min_int=repeat([min_int],size(t0_mat,1))
    Sig_inc_thresh=repeat([sig_inc_thresh],size(t0_mat,1))
    S2N1=repeat([S2N],size(t0_mat,1))
    Min_peak_w_s=repeat([min_peak_w_s],size(t0_mat,1));
    
    

    println("Files are loaded. The feature detection is starting. It may take several minutes.")

    if method == "S3DP"


        pmap(SAFD.safd_s3D,mz_vales_mat,mz_int_mat,Rt_mat,m_mat,pathin_mat,Max_it,Max_t_peak_w,Res,
            Min_ms_w,R_thresh,Min_int,Sig_inc_thresh,S2N1,Min_peak_w_s;retry_delays = zeros(2))

    elseif method == "3DP"

        pmap(SAFD.safd,mz_vales_mat,mz_int_mat,Rt_mat,m_mat,pathin_mat,Max_it,Max_t_peak_w,Res,
            Min_ms_w,R_thresh,Min_int,Sig_inc_thresh,S2N1,Min_peak_w_s;retry_delays = zeros(2))

    elseif method == "S3DC"

        println("This method has not been implemented yet. Stay tuned!")


    end 


    return 1

    
end 





########################
# test area 


"""

pathin = "/Users/saersamanipour/Desktop/dev/pkgs/MasSpecToolBox.jl/test"
filenames=["test_chrom.mzXML"]
mz_thresh=[0,600]
int_thresh = 150 

max_numb_iter=10
max_t_peak_w=300
res=20000
min_ms_w=0.02
r_thresh=0.85
min_int=2000
sig_inc_thresh=5
S2N=2

min_peak_w_s=3
method = "S3DP"

"""

