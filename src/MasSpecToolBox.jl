module MasSpecToolBox

using Distributed 
addprocs()


using Pkg 
#Pkg.update()
using CSV
using MS_Import
using SAFD
using CompCreate
using ULSA


# Write your package code here.

include("FeatureDetection.jl")


export feature_detect, feature_detect_batch, feature_detect_batch_dist




end
